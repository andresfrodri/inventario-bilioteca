from django.apps import AppConfig


class RecomendadosappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'recomendadosApp'
